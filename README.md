Danfoss
==========================================

В этом репозитории хранится веб-приложение
Веб-приложени написано с помощью фреймворка [quasar-framework][1]
который основан на Vue.js

Инструкция по использованию
---------------------------

Установка рабочего окружения [quasar-cli][2]:

    npm install quasar-cli -g
    npm install

Запуск сервера для разработки:

    quasar dev

Собрать приложение:

    quasar build

Все команды приложений Quasar CLI:

    quasar info

Запуск линтера [JavaScript style guide][3]

    npm run lint -- --fix

Генерация документации производится командой:

    npm run docs

Внутреннее устройство
---------------------

```
|-- danfos
    |-- docs
    |-- dist
    |-- src
    |   |-- assets
    |   |-- components
    |   |-- constants
    |   |-- css
    |   |-- layouts
    |   |-- pages
    |   |-- plugin
    |   |-- router
    |   |-- statics
    |   |-- store
    |   |-- types
    |   |-- App.vue
    |   |-- index.template.html
    |-- README.md
    |-- package.json
    |-- quasar.conf.js
```

- **docs** - сгенерированная документация по проекту
- **dist** - папка для хранения скомпилированных файлов
- **src** - весь код программы
- **src/assets** - файлы требующие обработку (настраивается в quasar.conf.js)
- **src/components** - UI-компоненты
- **src/constants** - полезные константы
- **src/css** - CSS-стили приложения
- **src/layouts** - шаблоны приложения
- **src/pages** - страницы приложения
- **src/plugin** - дополниетльные плагины. Ajax-запросы при помощи библиотеки [axios][4]
- **src/router** - роутер
- **src/statics** - файлы не требующие обработки
- **src/store** - Vuex
- **src/types** - поьзовательские типы

[1]: https://quasar-framework.org/
[2]: https://quasar-framework.org/guide/quasar-cli.html
[3]: https://github.com/standard/standard
[4]: https://www.npmjs.com/package/axios