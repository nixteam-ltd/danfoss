const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const fileUpload = require('express-fileupload');
const cors = require('cors')

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
app.use(fileUpload());

app.all('/', async (req, res) => {
    res.send('OK');
});

app.post('/upload', async (req, res) => {
    if (req.files) {
        const keys = Object.keys(req.files)
        keys.forEach(key => {
            req.files[key].mv(`${__dirname}/uploades/${req.files[key].name}`, function(err) {
                console.log(err)
            });
        });
    }

    res.send('OK');
});

module.exports = app;
