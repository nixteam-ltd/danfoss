const app = require('./app');
const config = require('./conf');

app.listen(config.PORT, () =>
    console.log(`Example app listening on port ${config.PORT}!`)
);

