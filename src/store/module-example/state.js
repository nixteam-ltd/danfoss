import Filter from '../../types/Filter.js'
import Answers from '../../types/Answers.js'

export default {
  filter: new Filter(),
  answers: new Answers()
}
