import PropTypes from 'prop-types'

export default class User {
  constructor (options) {
    Object.assign(this, options)

    PropTypes.checkPropTypes(User.propTypes, this)
  }

  /**
   * Десериализация данных от бэкенда.
   *
   * Применяется если  данные фильтра хранятся на бекенде
   * МЕТОД ДЛЯ ПРИМЕРА
   *
   * @param {Object} proxy - Поля бэкендовской JSON структуры.
   * @return {User} Разобранный объект.
   */
  static fromApi (proxy) {
    return new User({
      name: proxy.name,
      surname: proxy.name,
      email: proxy.mail
    })
  }

  /**
   * Cериализация данных для бэкенда.
   *
   * МЕТОД ДЛЯ ПРИМЕРА
   *
   * @return {Object} Набор параметров
   */
  toApi () {
    return {
      name: this.name,
      surname: this.selectedDates,
      email: this.selectedChecbox,
      password: this.password
    }
  }

  /**
   *  Проверить пароль.
   *
   * @return {Bool} true пароли совпадают
   */
  checkPassword () {
    return this.password && this.password === this.repeatPassword
  }

  /**
   * Проверить email.
   *
   * @return {Bool}  true email валидный
   */
  checkEmail () {
    let rEmail = /^([a-z0-9_.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i
    return rEmail.test(this.email)
  }
}

User.propTypes = {
  name: PropTypes.string,
  surname: PropTypes.string,
  email: PropTypes.string,
  password: PropTypes.string,
  repeatPassword: PropTypes.string
}
