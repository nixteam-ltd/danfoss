import PropTypes from 'prop-types'

export default class Filter {
  constructor (options) {
    /**
     * Фильры выбранные в listChecbox
     *
     * @type {Object}
     */
    this.selectedChecboxFilters = {}
    /**
     * Фильры выбранные в dateRange
     *
     * @type {Object}
     */
    this.selectedDates = {}
    /**
     * Выбранные  сhecbox в фильрах
     *
     * @type {Object}
     */
    this.selectedChecbox = {}

    Object.assign(this, options)

    PropTypes.checkPropTypes(Filter.propTypes, this)
  }

  /**
   * Десериализация данных от бэкенда.
   *
   * Применяется если  данные фильтра хранятся на бекенде
   * МЕТОД ДЛЯ ПРИМЕРА
   *
   * @param {Object} proxy - Поля бэкендовской JSON структуры.
   * @return {Filter} Разобранный объект.
   */
  static fromApi (proxy) {
    return new Filter({
      selectedChecboxFilters: proxy.selectedChecboxFilters,
      selectedDates: proxy.selectedDates,
      selectedChecbox: proxy.selectedChecbox
    })
  }

  /**
   * Cериализация данных для бэкенда.
   *
   * МЕТОД ДЛЯ ПРИМЕРА
   *
   * @return {Object} Набор параметров
   */
  toApi () {
    return {
      selectedChecboxFilters: this.selectedChecboxFilters,
      selectedDates: this.selectedDates,
      selectedChecbox: this.selectedChecbox
    }
  }
}

Filter.propTypes = {
  selectedChecboxFilters: PropTypes.any,
  selectedChecbox: PropTypes.any,
  selectedDates: PropTypes.any
}
