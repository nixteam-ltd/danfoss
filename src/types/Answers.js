import PropTypes from 'prop-types'

export default class Answers {
  constructor (options) {
    /**
     * Ответы на вопросы
     *
     * @type {Object}
     */
    this.answers = {}
    /**
     * Выбранные кнопки
     *
     * @type {Object}
     */
    this.selectedBtn = {}
    /**
     * Текст в вопросах
     *
     * @type {Object}
     */
    this.customText = {}

    Object.assign(this, options)

    PropTypes.checkPropTypes(Answers.propTypes, this)
  }

  /**
   * Десериализация данных от бэкенда.
   *
   * Применяется если  данные фильтра хранятся на бекенде
   * МЕТОД ДЛЯ ПРИМЕРА
   *
   * @param {Object} proxy - Поля бэкендовской JSON структуры.
   * @return {Answers} Разобранный объект.
   */
  static fromApi (proxy) {
    return new Answers({
      answers: proxy.answers,
      selectedBtn: proxy.selectedBtn,
      customText: proxy.customText
    })
  }

  /**
   * Cериализация данных для бэкенда.
   *
   * МЕТОД ДЛЯ ПРИМЕРА
   *
   * @return {Object} Набор параметров
   */
  toApi () {
    return {
      answers: this.answers,
      selectedBtn: this.selectedBtn,
      customText: this.customText
    }
  }
}

Answers.propTypes = {
  answers: PropTypes.any,
  selectedBtn: PropTypes.any,
  customText: PropTypes.any
}
