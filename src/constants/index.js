/**
 * Интервал обновления между последовательным запросами на поиск.
 */
export const SEARCH_INTERVAL = 500

/**
 * Количество миллисекунд работы анимации при загрузке приложения
 */
export const APP_START_INTERVAL = 1000

/**
 *  Количество показанных запросов
 */
export const SHOW_CHAT_ROOMS = 4

/**
 *  Количество звезд в рейтенге
 */
export const COUNT_RATING_STAR = 5

/**
 *  Количество байт в мегабайте
 */
export const BYTE_IN_MEGABYTE = 1048576
