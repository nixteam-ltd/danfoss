/**
 *  Роутер
 */
const routes = [
  {
    path: '/',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/Index.vue') }]
  },
  {
    path: '/call',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/Call.vue') }]
  },
  {
    path: '/chat',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/Chat.vue') }]
  },
  {
    path: '/signUp',
    component: () => import('layouts/SingLayot.vue'),
    children: [{ path: '', component: () => import('pages/SignUp.vue') }]
  },
  {
    path: '/signIn',
    component: () => import('layouts/SingLayot.vue'),
    children: [{ path: '', component: () => import('pages/SignIn.vue') }]
  },
  {
    path: '/chatForm',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/ChatForm.vue') }]
  },
  {
    path: '/cooldCall',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/CooldCall.vue') }]
  },
  {
    path: '/device',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/Device.vue') }]
  },
  {
    path: '/dashboard',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/Dashboard.vue') }]
  },
  {
    path: '/profile',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/Profile.vue') }]
  },
  {
    path: '/adminres',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/AdminRes.vue') }]
  },
  {
    path: '/teacher',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/Teacher.vue') }]
  },
  {
    path: '/objectRes',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/ObjectRes.vue') }]
  },
  {
    path: '/object',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/Object.vue') }]
  },
  {
    path: '/dimension',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/Dimension.vue') }]
  },
  {
    path: '/admin',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/Admin.vue') }]
  },
  {
    path: '/bludeprint',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/Bludeprint.vue') }]
  },
  {
    path: '/user',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/User.vue') }]
  },
  {
    path: '/manager',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/Manager.vue') }]
  },
  {
    path: '/tasks',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/Tasks.vue') }]
  },
  {
    path: '/forum-main',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/ForumMain.vue') }]
  },
  {
    path: '/forum-topic-list/:id',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/ForumTopicList.vue') }]
  },
  {
    path: '/forum-topic/:id',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/ForumTopic.vue') }]
  },
  {
    path: '/docs',
    component: () => import('layouts/DanfosLayout.vue'),
    children: [{ path: '', component: () => import('pages/Docs.vue') }]
  }
]

if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
